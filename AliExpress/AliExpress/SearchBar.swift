//
//  SearchBar.swift
//  AliExpress
//
//  Created by Alessia Saviano on 11/12/21.
//

import SwiftUI

struct SearchBar: View {
    @Binding var text: String // the inputted search text
    @State private var isEditing = false
    
    var body: some View {
        Group {
            HStack {
                TextField("Search...", text: $text)
                    .padding(15)
                    .padding(.horizontal, 30)
                    .background(.white)
                    .cornerRadius(10)
                    .overlay(HStack { // Add the search icon to the left
                        Image(systemName: "magnifyingglass")
                            .foregroundColor(.black)
                            .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
                            .padding(.leading, 12)
                        Image(systemName: "camera.viewfinder")
                            .foregroundColor(.gray)
                            .frame(minWidth: 0, maxWidth: .infinity, alignment: .trailing)
                            .padding(.trailing, 12)
                        
                        // If the search field is focused, add the clear (X) button
                        if isEditing {
                            Button(action: {
                                self.text = ""
                            }) {
                                Image(systemName: "multiply.circle.fill")
                                    .foregroundColor(.gray)
                                    .padding(.trailing, 12)
                            }
                        }
                    }).padding(.horizontal, 20)
                    .onTapGesture {
                        self.isEditing = true
                    }
                
                // If the search field is focused, render the "Cancel" button
                // to the right that hides the search bar altogether
                if isEditing {
                    Button(action: {
                        self.isEditing = false
                        self.text = ""
                        // Dismiss the keyboard
                        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                    }) {
                        Text("Cancel")
                            .foregroundColor(.white)
                    }.padding(.trailing, 20)
                        .transition(.move(edge: .trailing))
                        .animation(.default)
                }
            }
            
        }
    }
}

struct SearchBar_Previews: PreviewProvider {
    @State static var text = ""
    static var previews: some View {
        SearchBar(text: $text)
            .previewLayout(.sizeThatFits)
    }
}
