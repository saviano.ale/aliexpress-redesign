//
//  InfoView.swift
//  AliExpress
//
//  Created by Alessia Saviano on 14/12/21.
//

import SwiftUI

struct InfoView: View {
//    @Enviroment(\.presentationMode) var presentationMode
    var image: String
    var title: String
    var price: String
    var icon: String
    var rate: String
    var payment: String
    var seller: String
    var delivery: String
    var color: String
    var size: String
    var material: String
    var brand: String
    
    
    var body: some View {
        ZStack {
            Color.grayMode
                .ignoresSafeArea()
            
            ScrollView {
                Section {
                    VStack(alignment: .leading) {
                        Image(image)
                            .resizable()
                            .scaledToFill()
                            .frame(height: 400)
                            .clipped()
                        
                        VStack(alignment: .leading) {
                            Text(title)
                                .font(.system(size: 17))
                                .foregroundColor(.secondary)
                            
                            HStack(alignment: .center) {
                                Text(price)
                                    .font(.system(size: 28))
                                    .fontWeight(.bold)
                                    .foregroundColor(.primary)
                                
                                Spacer()
                                
                                HStack(alignment: .center) {
                                    Image(systemName: icon)
                                        .font(.system(size: 15))
                                        .foregroundColor(.yellow)
                                    Text(rate)
                                        .font(.system(size: 15))
                                        .foregroundColor(.secondary)
                                }.padding(.top, 1)
                            }
                        }.padding(.horizontal, 16).padding(.top, 7)
                        
                        HStack {
                            Button(action: {
                                print("Buy Click")
                            }, label: {
                                Image(systemName: "cart.badge.plus")
                                    .foregroundColor(.white)
                                    .font(.system(size: 20).bold())
                                    .frame(width: 60, height: 60)
                                    .background(.orange)
                                    .cornerRadius(5)
                            })
                            
                            Button(action: {
                                print("Cart Click")
                            }, label: {
                                Text("Buy now")
                                    .foregroundColor(.white)
                                    .font(.system(size: 20).bold())
                                    .frame(maxWidth: .infinity, maxHeight: 60)
                                    .background(Color.accentColor)
                                    .cornerRadius(5)
                            })
                        }.padding(.horizontal, 16).padding(.top, 10)
                        
                    }.padding(.bottom, 16).background(Color.boxColor)
                    
                    VStack(alignment: .leading) {
                        Section(header: Text("Shipping")                            .font(.system(size: 20).bold())) {
                            
                            HStack {
                                VStack(alignment: .leading) {
                                    Text("Payment").padding(.vertical, 5)
                                    Text("Seller").padding(.vertical, 5)
                                    Text("Delivery").padding(.vertical, 5)
                                }.foregroundColor(.gray)
                                
                                VStack(alignment: .leading) {
                                    Text(payment).padding(.vertical, 5)
                                    Text(seller).padding(.vertical, 5)
                                    Text(delivery).padding(.vertical, 5)
                                }.padding(.leading, 60)
                                
                                Spacer()
                            }
                        }
                    }.padding(16).background(Color.boxColor)
                    
                    VStack(alignment: .leading) {
                        Section(header: Text("Details")
                                    .font(.system(size: 20).bold())) {
                            
                            HStack {
                                VStack(alignment: .leading) {
                                    Text("Color").padding(.vertical, 5)
                                    Text("Size").padding(.vertical, 5)
                                    Text("Material").padding(.vertical, 5)
                                    Text("Brand").padding(.vertical, 5)
                                }.foregroundColor(.gray)
                                
                                VStack(alignment: .leading) {
                                    Text(color).padding(.vertical, 5)
                                    Text(size).padding(.vertical, 5)
                                    Text(material).padding(.vertical, 5)
                                    Text(brand).padding(.vertical, 5)
                                }.padding(.leading, 60)
                                
                                Spacer()
                            }
                        }
                    }.padding(16).background(Color.boxColor)
                }
            }
//            .edgesIgnoringSafeArea(.top)
            
        }
        .navigationBarColor(backgroundColor: UIColor(Color.grayMode), tintColor: UIColor(Color.accentColor))
    }
    
}

struct Test_Previews: PreviewProvider {
    static var previews: some View {
        InfoView(image: "socks", title: "NICE SOCKS", price: "3,67€", icon: "star.fill", rate: "4.9", payment: "Free", seller: "Mimmo", delivery: "2034", color: "blu", size: "unisex", material: "cotton", brand: "Sort")
    }
}
