//
//  Home.swift
//  AliExpress
//
//  Created by Alessia Saviano on 11/12/21.
//

import SwiftUI

struct Home: View {
    @State var searchText = ""
    @State var isShowing = true
    
    @State var navigationViewIsActive: Bool = false
    @State var selectedModel : ProductProto? = nil
    
    var categoryStore : [CategoryProto] = [CategoryProto(icon: "dollarsign.circle", title: "Super Deals", color: Color.blue), CategoryProto(icon: "shippingbox", title: "Free Shipping", color: Color.yellow), CategoryProto(icon: "bus", title: "Fast Delivery", color: Color.purple), CategoryProto(icon: "gift", title: "New Arrivals", color: Color.green)]
    
    var productStore : [ProductProto] = [ProductProto(userId: "socks", image: "socks", title: "NEW FUNNY COLORFUL SOCKS", price: "1,50 €", icon: "star.fill", rate: "4.9", payment: "Free", seller: "Cainiao Super Economy Global", delivery: "Jan 17", color: "Red and white", size: "Unisex", material: "Cotton", brand: "YWHUANSEN"), ProductProto(userId: "ring", image: "ring", title: "NICE GOLD RING", price: "2,47 €", icon: "star.fill", rate: "3.8", payment: "Free", seller: "Cainiao Super Economy Global", delivery: "Feb 21", color: "Gold", size: "12/13", material: "Metal", brand: "RUSLP"), ProductProto(userId: "hat", image: "hat", title: "HAT FOR SUMMER", price: "5,59 €", icon: "star.fill", rate: "4.2", payment: "1,85 €", seller: "Cainiao Heavy Parcel Line", delivery: "Mar 02", color: "White", size: "54-58cm", material: "Straw", brand: "C.RIKA")]
    
    var recommendedStore : [ProductProto] = [ProductProto(userId: "shoes", image: "shoes", title: "COLORFUL NIKE SHOES", price: "25,40 €", icon: "star.fill", rate: "4.5", payment: "Free", seller: "mimmo", delivery: "2035", color: "Pastel yellow, pink and purple", size: "unisex", material: "cotton", brand: "Nike"), ProductProto(userId: "jacket", image: "jacket", title: "JEANS JACKET WITH DONUT", price: "10,80 €", icon: "star.fill", rate: "5.0", payment: "free", seller: "Cainiao Super Economy Global", delivery: "2035", color: "red", size: "unisex", material: "cotton", brand: "nike"), ProductProto(userId: "watch", image: "watch", title: "STEEL FASHION WATCH", price: "4,25€", icon: "star.fill", rate: "4.7", payment: "free", seller: "mimmo", delivery: "2035", color: "red", size: "unisex", material: "cotton", brand: "nike")]
    
    var body: some View {
        NavigationView {
            ZStack {
                Color.grayMode
                    .ignoresSafeArea()
                
                    VStack {
                        if selectedModel != nil {
                            NavigationLink(destination: InfoView(image: selectedModel!.image, title: selectedModel!.title, price: selectedModel!.price, icon: selectedModel!.icon, rate: selectedModel!.rate, payment: selectedModel!.payment, seller: selectedModel!.seller, delivery: selectedModel!.delivery, color: selectedModel!.color, size: selectedModel!.size, material: selectedModel!.material, brand: selectedModel!.brand), isActive: $navigationViewIsActive){ }
                        }
                    }.hidden()
                    
                VStack {
                    VStack {
                        Image("logo")
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .frame(width: 130, height: 30, alignment: .center)
                            .padding(.bottom, 10)
                        
                        SearchBar(text: $searchText)
                        
                    }
                    .padding(.vertical, 20)
                    .background(Color.accentColor)
                    
                    
                    ScrollView {
                        VStack(alignment: .leading) {
                            HStack {
                                ForEach(categoryStore) {
                                    i in Button(action: {
                                        // add to an array
                                    }) { Category(icon: i.icon, title: i.title, color: i.color)
                                    }
                                }.padding(.horizontal, 8)
                            }.padding(.horizontal, 12)
                            
                            // Top selection
                            Section(header: HStack {
                                Text("Top Selection")
                                    .font(.title2)
                                    .fontWeight(.medium)
                                Spacer()
                                Button(action: {
                                    // add to an array
                                }) {
                                    Text("VIEW ALL")
                                        .font(.footnote)
                                    .foregroundColor(Color.gray)}
                            }.padding(.horizontal, 15)
                                        .padding(.top, 40)
                                    
                                    
                            ) {
                                ScrollView(.horizontal, showsIndicators: false) {
                                    HStack {
                                        ForEach(productStore, id: \.self) {
                                            i in Button(action: {
                                                self.selectedModel = i
                                                self.navigationViewIsActive = true
                                            }) { Product(image: i.image, title: i.title, price: i.price, icon: i.icon, rate: i.rate)
                                            }
                                        }
                                    }.padding(.horizontal, 12)
                                }
                            }
                            
                            // Recommended for you
                            Section(header: HStack {
                                Text("Recommended for you")
                                    .font(.title2)
                                    .fontWeight(.medium)
                                Spacer()
                                Button(action: {
                                    // add to an array
                                }) {
                                    Text("VIEW ALL")
                                        .font(.footnote)
                                    .foregroundColor(Color.gray)}
                            }.padding(.horizontal, 15)
                                        .padding(.top, 40)
                            ) {
                                ScrollView(.horizontal, showsIndicators: false) {
                                    HStack {
                                        ForEach(recommendedStore, id: \.self) {
                                            i in Button(action: {
                                                self.selectedModel = i
                                                self.navigationViewIsActive = true
                                            }) { Product(image: i.image, title: i.title, price: i.price, icon: i.icon, rate: i.rate)
                                            }
                                        }
                                    }.padding(.horizontal, 12)
                                }.padding(.bottom, 30)
                            }
                        }
                    }
                }
                
            }.navigationBarTitle("Back", displayMode: .inline)
            .navigationBarHidden(true)
        }
    }
}

struct Home_Previews: PreviewProvider {
    static var previews: some View {
        Home()
    }
}
