//
//  AliExpressApp.swift
//  AliExpress
//
//  Created by Alessia Saviano on 11/12/21.
//

import SwiftUI

extension Color {
    static let grayMode = Color("GrayMode")
    static let boxColor = Color("BoxColor")
}

@main
struct AliExpressApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
