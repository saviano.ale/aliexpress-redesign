//
//  Category.swift
//  AliExpress
//
//  Created by Alessia Saviano on 11/12/21.
//

import SwiftUI

struct CategoryProto: Identifiable {
    var id  = UUID()
    var icon: String
    var title: String
    var color: Color
}

struct Category: View {
    var icon: String
    var title: String
    var color: Color
    
    var body: some View {
        VStack {
            Image(systemName: icon)
                .foregroundColor(color)
                .font(.system(size: 28))
                .frame(width: 35, height: 35)
                .padding(.top, 10)
            Text(title)
                .font(.system(size: 15))
                .fontWeight(.bold)
                .multilineTextAlignment(.center)
                .foregroundColor(color)
                .frame(width: 65)
                .padding(.horizontal, 5)
                .padding(.bottom, 10)
        }.background(Color.boxColor).clipShape(RoundedRectangle(cornerRadius: 5))
            .padding(.top, 30)
    }
}

struct Category_Previews: PreviewProvider {
    static var previews: some View {
        Category(icon: "dollarsign.circle", title: "Free Shipping", color: Color.blue)
            .previewLayout(.sizeThatFits)
    }
}
