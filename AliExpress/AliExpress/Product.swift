//
//  Product.swift
//  AliExpress
//
//  Created by Alessia Saviano on 12/12/21.
//

import SwiftUI

struct ProductProto: Identifiable, Equatable, Hashable {
    var id = UUID()
    var userId: String
    var image: String
    var title: String
    var price: String
    var icon: String
    var rate: String
    var payment: String
    var seller: String
    var delivery: String
    var color: String
    var size: String
    var material: String
    var brand: String
}

struct Product: View {
    var image: String
    var title: String
    var price: String
    var icon: String
    var rate: String
    
    var body: some View {
        VStack(alignment: .leading) {
            Image(image)
                .resizable()
                .scaledToFill() // add if you need
                .frame(height: 164) // as per your requirement
                .clipped()
            
            VStack(alignment: .leading) {
                Text(title)
                    .font(.system(size: 15))
                    .foregroundColor(.secondary)
                    .lineLimit(1)
                
                HStack(alignment: .center) {
                    Text(price)
                        .font(.system(size: 17))
                        .fontWeight(.bold)
                        .foregroundColor(.primary)
                    
                    Spacer()
                    
                    HStack(alignment: .center) {
                        Image(systemName: icon)
                            .font(.system(size: 12))
                            .foregroundColor(.yellow)
                        Text(rate)
                            .font(.system(size: 12))
                            .foregroundColor(.secondary)
                    }
                }.padding(.top, 1)
            }.padding(.horizontal, 10).padding(.bottom, 10)
        }.background(Color.boxColor).clipShape(RoundedRectangle(cornerRadius: 5))
            .frame(width: 136)
            .padding(.horizontal, 7)
    }
}

struct Product_Previews: PreviewProvider {
    static var previews: some View {
        Product(image: "socks", title: "Free Shipping", price: "25,59 €", icon: "star.fill", rate: "4.9")
            .previewLayout(.sizeThatFits)
    }
}
